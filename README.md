# weddpics-client
Weddpics is a simple photo gallery. The goal is to enable newlyweds to manage the photos sent by guests.

API Documentation: https://weddpics.herokuapp.com/api/docs

Technologies used:
## Frontend:
- VueJS
- Vuetifyjs
- Vuex

## Backend:
- Python
- Flask
- MongoDB
- AWS S3


I this repository is present the frontend code for Weddpics API

API repository: https://gitlab.com/glaubersilva/weddpics



# app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).