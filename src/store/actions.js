import * as types from "./mutation-types";
import http from "../services/http-common";
import * as storage from "../services/storage";

export const ActionSetUser = ({ commit }, payload) => {
  commit(types.SET_USER, payload);
};

export const ActionSetToken = ({ commit }, payload) => {
  storage.setHeaderToken(payload);
  storage.setLocalToken(payload);
  commit(types.SET_TOKEN, payload);
};

export const ActionCheckToken = ({ dispatch, state }) => {
  if (state.token) {
    return Promise.resolve(state.token);
  }

  const token = storage.getLocalToken();
  if (!token) {
    return Promise.reject(new Error("Invalid Token"));
  }

  dispatch("ActionSetToken", token);
};

export const ActionDoLogin = ({ dispatch }, payload) => {
  return http
    .post("/users/login", {
      email: payload.email,
      password: payload.password,
    })
    .then((response) => {
      dispatch("ActionSetUser", response.data.user);
      dispatch("ActionSetToken", response.data.token);
    });
};

export const ActionDoRegister = ({ dispatch }, payload) => {
  return http
    .post("users/signup", {
      name: payload.name,
      surname: payload.surname,
      email: payload.email,
      password: payload.password,
    })
    .then((response) => {
      dispatch("ActionSetUser", response.data.user);
      dispatch("ActionSetToken", response.data.token);
    });
};

export const ActionSignOut = ({ dispatch }) => {
  storage.setHeaderToken("");
  storage.deleteLocalToken();
  dispatch("ActionSetUser", {});
  dispatch("ActionSetToken", {});
};
