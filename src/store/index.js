import * as actions from "./actions";
import state from "./state";
import mutations from "./mutations";
import * as getters from "./getters";

// import http from "../services/http-common";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  actions,
  getters,
  state,
  mutations,
});
