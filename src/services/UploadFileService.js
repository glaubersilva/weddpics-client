import http from "./http-common.js";

class UploadFilesService {
  upload(file, onUploadProgress) {
    let formData = new FormData();

    formData.append("file", file);
    formData.set("description", file.description);
    console.log("FORMDATA", formData);

    return http.post("/photos/", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  }
  getFiles() {
    return http.get("/photos/");
  }
}
export default new UploadFilesService();
