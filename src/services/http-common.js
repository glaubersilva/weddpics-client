import axios from "axios";

axios.defaults.baseURL = "https://weddpics.herokuapp.com/api";
axios.defaults.headers = { "Content-type": "application/json" };

const setBearerToken = (token) => {
  axios.defaults.headers["Authorization"] = `Bearer ${token}`;
};

export default axios;
export { setBearerToken };
