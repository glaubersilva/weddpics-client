import store from "../store";

export default async (to, from, next) => {
  if (to.name == "about") {
    next();
  } else {
    if (to.name !== "auth" && !store.getters["hasToken"]) {
      try {
        await store.dispatch("ActionCheckToken");
        next({ name: "home" });
      } catch (error) {
        next({ name: "auth" });
      }
    } else {
      if (to.name === "auth" && store.getters["hasToken"]) {
        next({ name: "home" });
      } else {
        next();
      }
    }
  }
};
