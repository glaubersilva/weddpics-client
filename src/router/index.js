import Vue from "vue";
import VueRouter from "vue-router";
import beforeEach from "./beforeEach";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/upload",
    name: "upload",
    component: () => import("../views/Upload"),
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../views/About"),
  },
  {
    path: "/photos/:id",
    name: "photo",
    component: () => import("../views/Photo"),
  },
  {
    path: "*",
    name: "notfound",
    component: () => import("../views/NotFound"),
  },
  {
    path: "/auth",
    name: "auth",
    component: () => import("../views/Auth"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(beforeEach);

export default router;
